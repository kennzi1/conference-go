from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

def update_AccountVO(ch, method, properties, body):
    content = json.loads(body)
    content['updated'] = datetime.fromisoformat(content['updated'])
    if content['is_active']:
    #     Use the update_or_create method of the AccountVO.objects QuerySet
        AccountVO.objects.update_or_create(
            **content,
            defaults={"email": content["email"]}
        )
    #         to update or create the AccountVO object
    else:
    #     Delete the AccountVO object with the specified email, if it exists
        AccountVO.objects.filter(email=content["email"]).delete()


while True:
    try:
        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.exchange_declare(exchange='account_info',
            exchange_type='fanout')
            result = channel.queue_declare(queue='', exclusive=True)
            queue_name = result.method.queue
            channel.queue_bind(exchange='account_info',
            queue=queue_name)
            channel.basic_consume(
              queue=queue_name,
              on_message_callback=update_AccountVO,
              auto_ack=True
            )
            channel.start_consuming

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
