from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_pexels(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state, how many pictures per page, header
    params = {
      "per_page": 1,
      "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url":None}

def get_lat_lon(city, state):
    # Create the URL for the geocoding API with the city and state
    params = {
      "q": f'{city},{state},USA',
      "limit": 1,
      "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    location_data = response.json()
    # Get the latitude and longitude from the response
    lat = location_data[0]["lat"]
    lon = location_data[0]["lon"]

    return lat, lon  # returns a tuple

def get_weather(city, state):
    lat, lon = get_lat_lon(city, state)  # unpacks the tuple
    # Create the URL for the current weather API with the latitude
    #   and longitude
    params = {
      "lat": lat,
      "lon": lon,
      "appid": OPEN_WEATHER_API_KEY,
      "units": "imperial"
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    weather_data = response.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    return {
      "temp": weather_data["main"]["temp"],
      "description": weather_data["weather"][0]["description"]
    }
